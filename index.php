<?php
/*
0. styling -> displayin sass use, not real styling ability | performed doezens of pixel perfect, more neat styling
1. didn't use uuid,
2. didn't have time for styling (IF YOU'll give me extra time to show my styling skills it would be nice)
3. only after build it so the request to keep visit counter
4. planed to use the id (unix timestamp ) as the entrenced time
5. the first doc. Tzlil sent me had a user/password, the second doc was a free name/email login so I just implant it as is, on session 
(no signup, no cookies, kept the empty db class though :) );
6. it will fail when someone close his browser.
7. STRACTURE:   the stracture is not a classic mvc:

    A) proce/proce.php -> not a restAPI but something I like to work with.
    B) class/... class files.
    C) inc/... usualy included files like header/footer/config files
    D) public/style|js|img ... public eyes files.
    
8. FILES:
    A) index.php            -> main app's file
    B) public/js/main.js    -> js object that runs it all on FE
    C) inc/config.php       -> session/version....
    D) proce/proce.php      -> procceess all ajax request 
    E) class/User.class.php -> the main class
    F) data/userList.txt    -> the data saved as JSON
*/

require_once './inc/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>session users...</title>
  
  <link href="public/style/main.css?v=<?=$version?>" rel="stylesheet">
</head>
<body>
  <header>
  <section id="hello"></div>
 </header>
  <main>
      <div id="userList"></div>
  </main>
  <aside>
  </aside>
  <footer>
  </footer>
  <script src="public/js/main.js?v=<?=$version?>" ></script>
</body>
</html>

