'user strict'
const CUR_SCRIPTS = document.getElementsByTagName("script"),
CUR_SCRIPT = CUR_SCRIPTS[CUR_SCRIPTS.length-1].src;

const mainObj={
    
    //copied from my own old code
    getDoamin(){
        return      (CUR_SCRIPT.includes('localhost'))?'http://localhost/ELEMENT/elementor_test':CUR_SCRIPT.split('.co.il')[0]+'.co.il';
      },

    //copied from my own old code
    //old general ajax function, gets url, object to send and callback function, no catch;
    postAjax(url, toSend, pType,success) {
        let params = typeof toSend == 'string' ? toSend : Object.keys(toSend).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(toSend[k]) }
        ).join('&');
        let xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xhr.withCredentials=true;
        xhr.open(pType, url);
        xhr.onreadystatechange = function() {
            if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText,toSend); }
        };
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
        return xhr;
    },
    //copied from my own old code
    // creates element pre dom implant
    newElem(obj){
        let e=document.createElement(obj.type);
        for (let attr in obj.attrList) {
          if (obj.attrList.hasOwnProperty(attr)) {
            e.setAttribute(attr, obj.attrList[attr]);
          }
        }
      return e;
      },
    // get interval that checks every 3 sec. or clear if logged out
    getCurOnline(){
        //you probably expected websockets......
        if(mainObj.isLogin){
            mainObj.onlineUsersIntrval=setInterval(()=>{
                mainObj.postAjax(mainObj.getDoamin()+'/proce/proce.php',{dw:'getCurOnline'},'POST',mainObj.postCheckOnline);
            }, 3000);
        }
        else{
            document.getElementById('userList').innerHTML='';
            clearInterval(mainObj.onlineUsersIntrval);
        }

    },
    //gets the online user list, update it in mainObj.curOnline and call a render func.
    postCheckOnline  (data){
       let d=JSON.parse(data);
       mainObj.curOnline=d.res=='success'?d.curOnline:[];
       mainObj.renderUserList();
    },

    // render curOnline list
    renderUserList(){        
        const listDiv=document.getElementById('userList');
        listDiv.innerHTML='';
        for (let i = 0; i < mainObj.curOnline.length; i++) {
            const user = mainObj.curOnline[i];
            let tmpElem= mainObj.newElem({type:'div',attrList:{class:"userBlock",id:`user_${user.id}`}});
            let tmpBut= mainObj.newElem({type:'button',attrList:{class:"userButton",value:`${user.id}`}});
            tmpBut.innerText='+';
            tmpBut.addEventListener('click',(e)=>{
                e.preventDefault();
                mainObj.popUserData(e.target.value);
              });
            tmpElem.innerHTML=`<div>${user.userName}</div>`;
            tmpElem.appendChild(tmpBut);
            listDiv.appendChild(tmpElem);
        } 
    },
    // convert unix timestamp to readable date and time;
    unixToDate(uNix){
        let date = new Date(uNix * 1000);
        let months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        let year = date.getFullYear();
        let month = months[date.getMonth()];
        let day = date.getDate();

        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        return `${day}.${month}.${year} at ${hours}:${minutes.substr(-2)}`;
    },
    //gets user data by id, from mainObj.curOnline, popup div with data
    popUserData(id){
        let userData=mainObj.curOnline.find(user=> user.id=id);
        let mainDiv= mainObj.newElem({type:'div',attrList:{class:"userBlock",id:`popDiv`}});
        let bgDiv= mainObj.newElem({type:'div',attrList:{class:"popBG",onclick:'mainObj.removePop()'}});
        let txtDiv= mainObj.newElem({type:'div',attrList:{class:"popTXT"}});
        // time constrainsת דםררט:
        txtDiv.innerHTML=`<button class="closeMe" onclick="mainObj.removePop()">X</button> 
        <div class="userList">
            <div class="userBlock"><div>name</div><div>${userData.userName}</div></div>
            <div class="userBlock"><div>user eMail</div><div>${userData.userMail}</div></div>
            <div class="userBlock"><div>id</div><div>${userData.id}</div></div>
            <div class="userBlock"><div>loggedin</div><div>${this.unixToDate(userData.id)}</div></div>
        </div>`;
        mainDiv.appendChild(bgDiv);
        mainDiv.appendChild(txtDiv);        
        document.body.appendChild(mainDiv)
    },
    //delete popup  
    removePop(){
        document.getElementById('popDiv').remove();
    },
    // render hello row+ form
    renderHello(data){
        let helloElem=document.getElementById('hello');
        let d=JSON.parse(data);
        mainObj.isLogin=d.displayForm?false:true;
        document.getElementById('userList').innerHTML='';
        helloElem.innerHTML='';        
        let tmpH2=mainObj.newElem({type:'h2',attrList:{}})
        tmpH2.innerText=`Hello ${d.userName}`;
        
        helloElem.appendChild(tmpH2);
        let tmpForm=mainObj.newElem({type:'form',attrList:{'method':'POST',id:'logForm','data-do':d.displayForm?'login':'logout'}})
        if(d.displayForm){
            let tmpUserName=mainObj.newElem(
                {type:'input',
                attrList:{
                    type:'text',
                    id:'userName',
                    pattern:'[a-zA-Z]{2,15}',
                    required:true,
                    placeholder:'user name (En only)'}}
            );
            let tmpUserMail=mainObj.newElem(
                {type:'input',
                attrList:{
                    type:'email',
                    id:'userMail',
                    required:true,
                    placeholder:'propper eMail'}}
            );
        tmpForm.appendChild(tmpUserName);
        tmpForm.appendChild(tmpUserMail);
        }
        let tmpSubmit=mainObj.newElem(
            {type:'input',
            attrList:{
                type:'submit',
                id:'dw',
                value:d.displayForm?'Login':'Logout'}}
        );
        tmpForm.appendChild(tmpSubmit);
        helloElem.appendChild(tmpForm);
        tmpForm.addEventListener('submit',mainObj.formSubmit)
        mainObj.getCurOnline();
    },
    //form submit (log in/out)
    formSubmit(event){
        event.preventDefault();
        let tmpForm=event.target;
        let tmpInputs=tmpForm.elements;
        let toSendObj={};
        for (let i = 0; i < tmpInputs.length; i++) {
            const elem = tmpInputs[i];
            toSendObj[elem.id]=elem.value;
        }
        mainObj.postAjax(mainObj.getDoamin()+'/proce/proce.php',toSendObj,'POST',mainObj.renderHello);
    },
    // initiate page;
    iniPage(){
        mainObj.postAjax(mainObj.getDoamin()+'/proce/proce.php',{dw:'iniPage'},'POST',mainObj.renderHello);
    },
}


document.addEventListener("DOMContentLoaded", function() {    
mainObj.iniPage();    
});
