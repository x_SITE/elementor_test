<?php 
/*
0. styling -> displayin sass use, not real styling ability | performed doezens of pixel perfect, more neat styling
1. didn't use uuid,
2. didn't have time for styling (IF YOU'll give me extra time to show my styling skills it would be nice)
3. only after build it so the request to keep visit counter
4. planed to use the id (unix timestamp ) as the entrenced time
5. the first doc. Tzlil sent me had a user/password, the second doc was a free name/email login so I just implant it as is, on session 
(no signup, no cookies, kept the empty db class though :) );
6. it will fail when someone close his browser, saw the request to do so only after finished...
7. STRACTURE:   the stracture is not a classic mvc:

    A) proce/proce.php -> not a restAPI but something I like to work with.
    B) class/... class files.
    C) inc/... usualy included files like header/footer/config files
    D) public/style|js|img ... public eyes files.
    
8. FILES:
    A) index.php            -> main app's file
    B) public/js/main.js    -> js object that runs it all on FE
    C) inc/config.php       -> session/version....
    D) proce/proce.php      -> procceess all ajax request 
    E) class/User.class.php -> the main class
    F) data/userList.txt    -> the data saved as JSON
*/

// require_once 'Db.class.php';

class User {
    // protected $db;
    function __construct() {
        //  $this->db = new Db();
         $this->fileName='../data/userList.txt';
    }
    
    //check if logged in- returns which form to display and user name
    function checkIfLogin(){
        $displayForm=false;
        if(!isset($_SESSION['userTmpID']) || !isset($_SESSION['userName']) || !isset($_SESSION['userMail'])){
            session_unset();
            $_SESSION['userName']='Guest';
            $displayForm=true;        
        }
        return [
            'res'=>'success',
            'userName'=>$_SESSION['userName'],
            'displayForm'=>$displayForm
        ];
    }
    //login and write to data
    function login($arr){
        $_SESSION['userName']=$arr['userName'] ; 
        $_SESSION['userMail']=$arr['userMail'] ;
        $_SESSION['userTmpID']=time();
        $curOnline=$this->getCurOnline(false)['curOnline'];
        $isIn = array_search($_SESSION['userTmpID'], array_column($curOnline, 'id'));
        if($isIn===false){
            $curOnline[]=$this->makeUserData();
            $this->updateCurOnlineData($curOnline);
        }
        return $this->checkIfLogin();
    }

    // logout and delete from data JSON
    function logout(){
        $curOnline=$this->getCurOnline()['curOnline'];
        $isIn = array_search($_SESSION['userTmpID'], array_column($curOnline, 'id'));
        if($isIn!==false ){
            unset($curOnline[$isIn]);
            $this->updateCurOnlineData($curOnline);
        }
        session_unset();
        return $this->checkIfLogin();
    }

    // creates array of user data to save to data JSON
    function makeUserData(){
        return [
            'id'=>$_SESSION['userTmpID'],
            'userName'=>$_SESSION['userName'],
            'userMail'=>$_SESSION['userMail'],
            'userAgent'=>$_SERVER['HTTP_USER_AGENT']
        ];
        
    }
    // update new array jSON in data file 
    function updateCurOnlineData($arr){
        $f = fopen($this->fileName, 'w');
        fwrite($f, json_encode($arr));
        fclose($f);
    }

    // if logged in return success and current online users, else erturn error and emty arr
    function getCurOnline($toWrite=true){
        if(!isset($_SESSION['userTmpID']) || !isset($_SESSION['userName']) || !isset($_SESSION['userMail'])){
                return [
                    'res'=>'error',
                    'curOnline'=>[],
                ];
        }
        if (!file_exists($this->fileName) && $toWrite) {
            $cont=[];
            $cont[]=$this->makeUserData();
            $this->updateCurOnlineData($cont);
          }
          return [
              'res'=>'success',
              'curOnline'=>json_decode(file_get_contents ($this->fileName),true)
          ];
          
    }

}
?>